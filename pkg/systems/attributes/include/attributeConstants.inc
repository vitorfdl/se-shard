/* $Id: attributeConstants.inc 834 2005-11-02 14:09:50Z austin $
 *
 * Purpose
 * Provide various functions and constants for the attribute system.
 *
 */
use uo;

enum VITALS
	HITS			    := "Hits",
	MANA			    := "Mana",
	STAMINA			 := "Stamina"
endenum

enum STATS
	STRENGTH		    := "Strength",
	DEXTERITY		 := "Dexterity",
	INTELLIGENCE	 := "Intelligence",
	CONSTITUTION	 := "Constitution",
	WISDOM	       := "Wisdom",
endenum

enum DIFFICULTIES
	DIFF_VERYEASY   := 8,
	DIFF_EASY       := 10,
	DIFF_MEDIUM     := 14,
	DIFF_HARD       := 17,
	DIFF_VERYHARD   := 20,
	DIFF_IMPOSSIBLE := 23,
endenum

enum SKILLS
	ALQUIMIA            := "Alquimia",
	LAMINAS_CURTAS      := "Lâminas_Curtas",
	ESGRIMA             := "Esgrima",
   ESPADAS             := "Espadas",
   LANCA_E_ALABARDA    := "Lança_e_Alabarda",
   MACHADOS            := "Machados",
   ARMAS_DE_CONCUSSAO  := "Armas_de_Concussão",
   ARMAS_DUPLAS       := "Armas_Duplas",
	ARMADURA_LEVE       := "Armadura_Leve",
	ARMADURA_MEDIA      := "Armadura_Média",
	ARMADURA_PESADA     := "Armadura_Pesada",
	ARCO                := "Arco",
	BESTA               := "Besta",
	DESARMADO           := "Desarmado",
	ESCUDO              := "Escudo",
	PRIMEIROS_SOCORROS  := "Primeiros_Socorros",
	VENEFICIO           := "Venefício",
	PERCEPCAO           := "Percepção",
	RASTREAMENTO        := "Rastreamento",
	PESCARIA            := "Pescaria",
	ESCALPELAR          := "Escalpelar",
	PLANTACAO           := "Plantação",
	MARCENARIA          := "Marcenaria",
	COZINHA             := "Cozinha",
	COSTURA             := "Costura",
	FERRARIA            := "Ferraria",
	EXTRACAO            := "Extração",
	TRATO_ANIMAIS       := "Trato_de_Animais",
	MONTARIA            := "Montaria",
	INSCRICAO_RUNICA    := "Inscrição_Rúnica",
	MILAGRES            := "Milagres",
	FURTO               := "Furto",
   HERALDICA           := "Heráldica",
	IMOBILIZACAO        := "Imobilização",
	FURTIVIDADE         := "Furtividade",
	SABOTAGEM 			  := "Sabotagem",
	INSTRUMENTO_MUSICAL := "Instrumento_Musical",
   MILAGRE_DE_NECROMANCIA := "Milagre_de_Necromância",
   MILAGRE_DE_CURA := "Milagre_de_Cura"
endenum

