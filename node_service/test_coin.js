(async () => {
   const cointypes = ['C', 'B', 'A'] 
   const multipler = 100;

   const origin = { "B": 101, "C": 15 };
   const target = { 'A': 1 };

   const origin_total_l_value = Object.keys(origin).reduce((total, x) => {
      let i = Math.pow(multipler, cointypes.indexOf(x));
      return total + (origin[x] * i);
   }, 0);
   
   const target_total_l_value = Object.keys(target).reduce((total, x) => {
      let i = Math.pow(multipler, cointypes.indexOf(x));
      return total + (target[x] * i);
   }, 0);

   if (target_total_l_value > origin_total_l_value) return console.log('false');
   
   let result = origin_total_l_value-target_total_l_value;
   const result_total = {};

   let pos = cointypes.length;
   while (result > 0 && pos >= 0) {
      const type = cointypes[pos-1];
      let i = Math.pow(multipler, pos-1);

      const total = Math.floor(result / i);
      if (total >= 1) {
         result_total[type] = total;
         result -= (total * i);
      }
      pos -=1;
   }

   console.log(result_total)
})();